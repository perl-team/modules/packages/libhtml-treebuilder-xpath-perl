libhtml-treebuilder-xpath-perl (0.14-2) UNRELEASED; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Jul 2012 12:34:00 -0600

libhtml-treebuilder-xpath-perl (0.14-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 13:21:33 +0100

libhtml-treebuilder-xpath-perl (0.14-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/copyright: Update years of copyright.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 01 Oct 2011 14:19:27 +0200

libhtml-treebuilder-xpath-perl (0.13-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.2 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 09 Jul 2011 00:17:16 +0200

libhtml-treebuilder-xpath-perl (0.12-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ Ansgar Burchardt ]
  * New upstream release.
  * Use minimal debian/rules.
  * Use source format 3.0 (quilt).
  * debian/copyright: Formatting changes; refer to "Debian systems" instead of
    "Debian GNU/Linux systems"; refer to /usr/share/common-licenses/GPL-1.
  * debian/control: Make build-dep on perl unversioned.
  * Bump Standards-Version to 3.9.1.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 28 Oct 2010 16:23:40 +0200

libhtml-treebuilder-xpath-perl (0.11-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Removed quilt patch as this issue is fixed upstream :-)
    (Closes: RT#46203)
    -> Remove README.source and quilt-related stuff
  * Upstream has updated docs
  * Added new dependencies for XML::XPathEngine
  * Upgraded to Standards-Version 3.8.1
  * Added machine-readable copyright file
  * Forwarded patch upstream (see RT#46203)
  * Clarified the description

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

 -- Jonathan Yu <frequency@cpan.org>  Tue, 19 May 2009 12:26:18 -0400

libhtml-treebuilder-xpath-perl (0.09-3) unstable; urgency=low

  * debian/rules: delete /usr/lib/perl5 only if it exists (closes: #468038).
  * debian/watch: improved regexp for matching upstream releases.
  * Add /me to Uploaders.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 08 Mar 2008 22:01:28 +0100

libhtml-treebuilder-xpath-perl (0.09-2) unstable; urgency=medium

  * Fix depends: s/xmlpath/xpath/.
    Urgency medium, as the package is uninstallable.

 -- Damyan Ivanov <dmn@debian.org>  Tue, 11 Dec 2007 09:49:05 +0200

libhtml-treebuilder-xpath-perl (0.09-1) unstable; urgency=low

  [ Jeremiah C. Foster ]
  * Initial Release. (closes: #441850).

 -- Damyan Ivanov <dmn@debian.org>  Tue, 04 Dec 2007 22:52:35 +0200
